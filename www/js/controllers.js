angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout,$state,$localStorage,$rootScope) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  

	
	
  $scope.LogOut = function()
  {
	  $localStorage.userid = '';
	  $localStorage.name = '';
	  $localStorage.email = '';
	  $state.go('app.deals');
  }
  
  
  $scope.supplierLogout = function()
  {
	  $localStorage.supplierid = '';
	  $localStorage.suppliername = '';
	  $localStorage.suppliermail = '';
	  $state.go('app.deals');	  
  }

  
})


.controller('DealsCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce,$ionicModal,$ionicLoading) 
{
	//$scope.$on('$ionicView.enter', function(e) 
  //	{
//		setTimeout(function()
//		{
	
	
	$scope.getDeals = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	
		//get sapakim
		$http.get($rootScope.Host + '/getSapakim.php').success(function(data)
		{
			$ionicLoading.hide();
			console.log('Sapakim: ',data);
			//$rootScope.Deals = [];
			//$rootScope.Suppliers = [];
			$scope.DealsSearchArray = data;
			
			//var Suppliers = JSON.stringify(data).replace(/\\/g, '');
			//$rootScope.Suppliers =  JSON.parse(Suppliers);
			$rootScope.Data = data;
			
			/*
			
			for(var z=0;z<$rootScope.Data.length;z++)
			{
				$rootScope.Suppliers.push($rootScope.Data[z])
				for(var j=0;j<$rootScope.Data[z].deals.length;j++)
				{
					$rootScope.Deals.push($rootScope.Data[z].deals[j])
				}
			}
			*/
			
			
			console.log("Suppliers",$rootScope.Suppliers)		
			console.log("Deals",$rootScope.Deals)
		});
		
	}	
	
	$scope.getDeals();
	
	
	$scope.host = $rootScope.Host;
	$scope.navTitle='<img class="title-image" src="img/logo.png" />'
	$scope.Deals = $rootScope.Deals;
	$rootScope.ControllerName = "deals";
	//$scope.DealsSearchArray = new Array();
	
	$scope.SelectedPrice = 0;
	
	
	
	/*
	$rootScope.$watch('Deals', function(value) 
	{
		$scope.DealsSearchArray = $rootScope.Deals;

	})
	*/
	

	$scope.search = 
	{
		"area" : "",
		"guests" : "300",
		"season" : "",
		"catagory" : "4"
	}
	
	/*
	$scope.search.area
	$scope.search.guests
	$scope.search.season
	$scope.search.catagory
	*/

	$scope.dealSearch = function()
	{
	  $ionicModal.fromTemplateUrl('templates/deal_search.html', {
		scope: $scope
	  }).then(function(dealSearchModal) {
		$scope.dealSearchModal = dealSearchModal;
		$scope.dealSearchModal.show();
	  });
	}
	$scope.closeModal = function()
	{
		$scope.dealSearchModal.hide();
	}

	
	$scope.getSupplierByIndex = function(id)
	{
		for(var i=0;i<$rootScope.Suppliers.length; i++)
		{
			if($rootScope.Suppliers[i].index == id)
			{
				return $rootScope.Suppliers[i].name;
			}
		}
		
		DealsArray = new Array();
		
		for(var i=0;i<$rootScope.Deals.length; i++)
		{
			if($rootScope.Suppliers[i].index == id)
			{
				DealsArray = $rootScope.Deals[i];
			}
		}
		
	
	}
	

	
	//search modal filter
	$scope.FilterSearchDeal = function()
	{
		$rootScope.dealGuests = $scope.search.guests;
		
		
		$scope.DealsSearchArray = new Array();
		for(var i=0;i<$rootScope.Deals.length; i++)
		{
			//console.log("deals info ",$rootScope.Deals[i])
			if($rootScope.Deals[i].catagory == $scope.search.catagory && ($rootScope.Deals[i].area == $scope.search.area || $scope.search.area == "") && ($rootScope.Deals[i].season == $scope.search.season || $scope.search.season == ""))
			{
				if ($rootScope.Deals[i].catagory == 4 && parseInt($scope.search.guests) >= parseInt($rootScope.Deals[i].minsize) && parseInt($scope.search.guests) <= parseInt($rootScope.Deals[i].maxsize))
				{

					//alert ($rootScope.Deals[i].maxsize)
					$scope.SelectedPrice = $scope.getSelectedPrice($rootScope.Deals[i],$scope.search.guests);
					$scope.dealPrice = (parseInt($scope.search.guests)*parseInt($scope.SelectedPrice))+parseInt($rootScope.Deals[i].fixedprice);
					$rootScope.Deals[i].price = $scope.dealPrice;
					$rootScope.Deals[i].old_price = $scope.dealPrice+($scope.dealPrice*($rootScope.Deals[i].discount_precent/100));
					
					$rootScope.dealOldPrice = $rootScope.Deals[i].old_price;
					$rootScope.dealNewPrice = $rootScope.Deals[i].price;
					//console.log($scope.dealPrice);
					$scope.DealsSearchArray.push($rootScope.Deals[i]);
				}
				if ($rootScope.Deals[i].catagory != 4)
				{
					$scope.DealsSearchArray.push($rootScope.Deals[i]);
				}
				
					//console.log('new deals: ',$rootScope.Deals[i]);
						
					
			}
		}	
		
		console.log("deal 1 " , $scope.DealsSearchArray)
		
		if ($scope.DealsSearchArray.length == 0)
		{
			$ionicPopup.alert({
			 title: 'לא נמצאו תוצאות אנא נסה שנית',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });					
		}
		else if ($scope.dealSearchModal)
		{
			$scope.dealSearchModal.hide();						
		}
	
	}
	

	
	$scope.checkPercent = function(deal)
	{
		z=deal.old_price-deal.price;
		z= z/deal.old_price;
		z = parseInt(z*100);
		//alert(parseInt(deal.price)*parseInt(deal.discount_precent/10))
		return z;
	}
	
	$scope.cutDate = function(date)
	{
		var dArray = date.split('/');
		dateStr = dArray[0]+"."+dArray[1]
		return dateStr;
	}
	
	$scope.cutYear = function(date)
	{
		var dArray = date.split('/');
		dateStr = dArray[2]
		return dateStr
	}
	
	$scope.navigateDeal = function(index,id,oldprice,newprice)
	{
		$state.go('app.dealsInfo', { ItemId: index });
		/*
		$rootScope.dealOldPrice = oldprice;
		$rootScope.dealNewPrice = newprice;		
		$state.go('app.dealsInfo', { ItemId: id });
		*/
	}
	
			
//		}, 300);
	//});
})

.controller('dealsInfoCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce,$ionicScrollDelegate) 
{
//$scope.$on('$ionicView.enter', function(e) 
//	{
//		setTimeout(function()
//		{
		$scope.host = $rootScope.Host;
		$scope.Deal = [];
		$scope.Supplier = [];
		$rootScope.ControllerName = "dealsinfo";
		
		$scope.dealId = $stateParams.ItemId;
		$scope.Deal = $rootScope.Data[$scope.dealId];
		$scope.Supplier = $scope.Deal.suppliers;
		
		$scope.SupplierGallery = $scope.Deal.gallery;
		
		
		
		
		console.log("deal data" ,$scope.Deal )
		
		
		/*
		$scope.dealGuests = $rootScope.dealGuests;
		$scope.dealNewPrice = $rootScope.dealNewPrice;
		$scope.dealOldPrice = $rootScope.dealOldPrice;	
		*/
		
		
		$scope.navTitle='<img class="title-image" src="img/logo.png" />'		
		

		$scope.details = 
		{
			"name" : $localStorage.name,
			"phone" : "",
			"email" : $localStorage.email,
			"text" : ""
		}

		/*
		for(var i=0;i<$rootScope.Deals.length;i++)
		{
			if( $rootScope.Deals[i].index == $stateParams.ItemId)
			{
				$scope.Deal = $rootScope.Deals[i];
			}
		}
		
		for(var i=0;i<$rootScope.Suppliers.length;i++)
		{
			if($rootScope.Suppliers[i].index == $scope.Deal.supplier_index)
			{
				$scope.Supplier = $rootScope.Suppliers[i];
				//console.log("SupplierInfo : " , $scope.Supplier)
			}
		}
		*/
		
		console.log("Supplier: ",$scope.Supplier)
		console.log("About");
		console.log($scope.Deal);
		$scope.hidedetails = 0;
		
		//$scope.DealIndex = $scope.Deal.index;
		//$scope.SupplierId = $scope.Supplier.index;
		//$scope.Phone = $scope.Supplier.phone;
		
		//console.log($scope.Supplier)
		
		$scope.getCategory = function()
		{
			for(var i=0;i<$rootScope.Data.length;i++)
			{
				if($scope.Supplier.category == $rootScope.Data[i].index)
				return $rootScope.Data[i].name;
			}
		}
		
		$scope.cutDate = function(date)
		{
			var dArray = date.split('/');
			dateStr = dArray[0]+"."+dArray[1]
			return dateStr;
		}
		
		$scope.cutYear = function(date)
		{
			var dArray = date.split('/');
			dateStr = dArray[2]
			return dateStr
		}

		$scope.sendForm = function()
		{

			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			
			if ($scope.details.name =="")
			{
					$ionicPopup.alert({
					 title: 'יש להזין שם מלא',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });
			}
			else if ($scope.details.phone =="")
			{
					$ionicPopup.alert({
					 title: 'יש להזין מספר טלפון',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });
			}
		/*
			else if ($scope.details.email =="")
			{
					$ionicPopup.alert({
					 title: 'יש להזין כתובת מייל',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });
			}
			else if ($scope.details.text =="")
			{
					$ionicPopup.alert({
					 title: 'יש להזין תוכל הפנייה',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });
			}
		*/
			else
			{
				
				send_data = 
				{
					//"id" : $scope.DealIndex,
					"id" : $scope.SupplierId,
					"deal_id" : $scope.DealIndex,
					"name" : $scope.details.name,
					"phone" : $scope.details.phone,
					"mail" : $scope.details.email,
					"text" : $scope.details.text,
					"isdeal" : 1,
					"send" : 1
					
				}					
				$http.post($rootScope.Host+'/send_offer.php',send_data).success(function(data)
				{
				});						
				
					$ionicPopup.alert({
					 title: 'תודה , פרטיך התקבלו בהצלחה נציג מטעם הספק יחזור אליך בהקדם האפשרי',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });					
				
				$scope.hidedetails = 0;
				
				
			}	
			
			
		}
		
		
		$scope.sendInfo = function()
		{
			$ionicScrollDelegate.scrollTop();
			$scope.hidedetails = 1;
			
		}
		
		$scope.closeDiv = function()
		{
			$scope.hidedetails = 0;
		}
		
		$scope.openChat = function()
		{
			$state.go('app.chat', { SupplierIndex: $scope.SupplierId, UserId: 4 });
		}
		
		$scope.PhoneDeal = function()
		{
			window.location.href="tel://"+$scope.Phone;

		}
		
		$scope.AddFavorite= function()
		{
			
			send_params = 
			{
					"user" : $localStorage.userid,
					"article" : $scope.DealIndex,
					"is_deal" : 1
			}
			//console.log(chat_params)
			$http.post($rootScope.Host+'/add_favorite.php',send_params)
			.success(function(data, status, headers, config)
			{
				$ionicPopup.alert({
				 title: 'נוסף בהצלחה למועדפים',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });
			})
			.error(function(data, status, headers, config)
			{

			});				
			
		}

			
//		}, 300);
	//});
})





.controller('SupplierLoginCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce,$ionicHistory,$ionicLoading) 
{

    if ($localStorage.supplierid)
    {
        $ionicHistory.nextViewOptions({
                disableAnimate: true,
                expire: 300
            });
    
        $state.go('app.supplierpanel');
    }


	
	$scope.login = 
	{
		"email" : "",
		"password" : ""
	}
	
	
	$scope.LoginSupplier = function()
	{
		if ($scope.login.email =="")
		{
			$ionicPopup.alert({
				 title: 'יש להזין שם משתמש',
				 template: ''
			});			
		}
		else if ($scope.login.password =="")
		{
			$ionicPopup.alert({
				 title: 'יש להזין סיסמה',
				 template: ''
			});			
		}
		else
		{
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	
		
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			send_params = 
			{
				"username" : $scope.login.email,
				"password" : $scope.login.password
			}
			//console.log(login_params)
			$http.post($rootScope.Host+'/supplier_login.php',send_params)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();
				
				if (data.response.status == 0)
				{
					$ionicPopup.alert({
						 title: 'שם משתמש או סיסמה שגוים יש לנסות שוב',
						 template: ''
					});		

					$scope.login.password = '';
				}
				else
				{
					$localStorage.supplierid = data.response.userid;
					$localStorage.suppliername = data.response.name;
					$localStorage.suppliermail = data.response.email;
					
					$scope.login.email = '';
					$scope.login.password = '';
					
					
					window.location ="#/app/supplierpanel";
					
				}
			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});				
		}
	}
	
	
  
})

.controller('SupplierPanelCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce,$ionicHistory,$ionicLoading) 
{
	$scope.suppliername = $localStorage.suppliername;
})



.controller('ManageDealCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce,$ionicHistory,$ionicLoading,dateFilter) 
{
	$scope.dealId = $stateParams.ItemId;
	$scope.Id = "";
	
	
	$scope.fields = 
	{
		"title" : "",
		"dealdate" : "",
		"regprice" : "",
		"discount" : ""
	}

	
	if ($scope.dealId == -1)
	{
		$scope.Url = "add_deal.php";
	}
	else
	{
		$scope.Url = "edit_deal.php";
		
		
		$scope.dealData = $rootScope.dealListJson[$scope.dealId];
		
		
		$scope.fields.title = $scope.dealData.title;
		$scope.fields.dealdate = $scope.dealData.deal_date;
		$scope.fields.regprice = $scope.dealData.old_price;
		$scope.fields.discount = $scope.dealData.price;
		$scope.Id = $scope.dealData.index;
		//alert ($scope.Id);
	}
	
	

	
	$scope.saveDeal = function()
	{
		if ($scope.fields.title =="")
		{
			$ionicPopup.alert({
				 title: 'יש להזין כותרת',
				 template: ''
			});			
		}
		else if ($scope.fields.dealdate =="")
		{
			$ionicPopup.alert({
				 title: 'יש להזין תאריך הדיל',
				 template: ''
			});			
		}
		else if ($scope.fields.regprice =="")
		{
			$ionicPopup.alert({
				 title: 'יש להזין מחיר רגיל',
				 template: ''
			});			
		}
		else if ($scope.fields.discount =="")
		{
			$ionicPopup.alert({
				 title: 'יש להזין מחיר מבצע',
				 template: ''
			});			
		}
		else
		{
			$scope.newdate = dateFilter($scope.fields.dealdate, 'dd/MM/yyyy');
			
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	
		
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			send_params = 
			{
				"supplierid" : $localStorage.supplierid,
				"id" : $scope.Id,
				"title" : $scope.fields.title,
				"dealdate" : $scope.newdate,
				"regprice" : $scope.fields.regprice,
				"discount" : $scope.fields.discount,
				
			}
			//console.log(login_params)
			$http.post($rootScope.Host+'/'+$scope.Url,send_params)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();

				
				window.location ="#/app/deallist";
					
				
			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});		
			
			
		}
	}
})

.controller('DealListCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce,$ionicHistory,$ionicLoading,dateFilter) 
{
	
	$scope.getSupplierDeals = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	
	
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		send_params = 
		{
			"supplierid" : $localStorage.supplierid,
		}
		//console.log(login_params)
		$http.post($rootScope.Host+'/get_supplier_deals.php',send_params)
		.success(function(data, status, headers, config)
		{
			$ionicLoading.hide();
			$scope.supplierDeals = data;
			$rootScope.dealListJson = data;

		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();
		});			
	}

	$scope.getSupplierDeals();
	
	
	$scope.deleteDeal = function(index,id)
	{
		
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקה?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) {
		if(res) 
		{

	
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	

						
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"supplierid" : $localStorage.supplierid,
				"id" : id
			}
			//console.log(login_params)
			$http.post($rootScope.Host+'/delete_deal.php',send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicLoading.hide();
				$scope.supplierDeals.splice(index, 1);	
			

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});	
				
		} 
		 else 
		 {
		 }
	   });
	}

})	
.filter('cutString_TitlePage', function () {
    return function (value, wordwise, max, tail) 
	{
		value =  value.replace(/(<([^>]+)>)/ig,"");
        if (!value) return '';
		
        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
    //    if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
     //   }
        return value + (tail || ' ');
    };
})

.filter('stripslashes', function () {
    return function (value) 
	{
	    return (value + '')
		.replace(/\\(.?)/g, function(s, n1) {
		  switch (n1) {
			case '\\':
			  return '\\';
			case '0':
			  return '\u0000';
			case '':
			  return '';
			default:
			  return n1;
		  }
		});
    };
})


.filter('checkImgUrl', function ($rootScope,$localStorage) {
    return function (value) 
	{
		var Img = "";
		
		if ($rootScope.SupplierDetails.email == $rootScope.Suppliers[value.userid-1].email)
		{
			Img = $rootScope.Host + $rootScope.SupplierDetails.image;
		}
		else if(value)
		{
			if(value.image.charAt(0) == "u")
			{
				Img = $rootScope.Host + "/"+value;
			}
			else
			{
				Img = "img/chat/avatar.gif";
			}	
		}
		else
		{
			Img = "img/chat/avatar.gif";
		}
	
	
		console.log(Img)
		return Img;
		
    };
	
})

.filter('lastseenDate', function ($rootScope) {
    return function (value) 
	{
		if(value != "s")
		{
			var splitLastSeen = '';
		
			if (value.lastLogin)
			splitLastSeen =  value.lastLogin.split(" ");
			else if (value.date)
			splitLastSeen =  value.date.split(" ");

			var date = splitLastSeen[0];
			var hour = splitLastSeen[1];
		
			var splitdate =  date.split("-");
			var splithour =  hour.split(":");
		
			return ''+splitdate[2]+'/'+splitdate[1]+'/'+splitdate[0]+' '+ splithour[0]+':'+splithour[1]+'';
		}
		
    };
})




