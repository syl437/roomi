// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','ngStorage','openfb','ngSanitize','starter.factories'])


.run(function($ionicPlatform,$rootScope,OpenFB,$http) {
	

//OpenFB.init('365225076964472');	
$rootScope.Suppliers = "";
$rootScope.Catagoryname = "";
$rootScope.ControllerName = "";
$rootScope.Favorites = [];
$rootScope.likedArticle = [];
$rootScope.dealListJson = [];
$rootScope.dealOldPrice = "";
$rootScope.dealNewPrice = "";
$rootScope.dealGuests = "300";
$rootScope.Host = 'http://tapper.co.il/roomi/php/';

/*
    $rootScope.pusher = new Pusher('aabe1a626385b618b6fd', {
      encrypted: false
    });
*/

	
	
	/*
	
	//get blog posts
	$http.get($rootScope.Host + '/getBlog.php').success(function(data)
	{
		$rootScope.BlogPosts = data.response;
		//console.log("s1")
		//console.log($rootScope.Suppliers)
	});
	*/
	
	


  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
	$ionicConfigProvider.backButton.previousTitleText(false).text('');
	
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })


  
  .state('app.deals', {
    url: '/deals',
    views: {
      'menuContent': {
        templateUrl: 'templates/deals.html',
		 controller: 'DealsCtrl'

      }
    }
  })
  
 
  .state('app.supplierlogin', {
    url: '/supplierlogin',
    views: {
      'menuContent': {
        templateUrl: 'templates/supplierlogin.html',
		 controller: 'SupplierLoginCtrl'

      }
    }
  }) 

  .state('app.deallist', {
    url: '/deallist',
    views: {
      'menuContent': {
        templateUrl: 'templates/deallist.html',
		 controller: 'DealListCtrl'

      }
    }
  }) 
  
  

  
  .state('app.dealsInfo', {
    url: '/dealsInfo/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/deals_info.html',
		 controller: 'dealsInfoCtrl'

      }
    }
  })
  
  
  .state('app.supplierpanel', {
    url: '/supplierpanel',
    views: {
      'menuContent': {
        templateUrl: 'templates/supplierpanel.html',
		 controller: 'SupplierPanelCtrl'

      }
    }
  })   

  
  .state('app.managedeal', {
    url: '/managedeal/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/managedeal.html',
		 controller: 'ManageDealCtrl'

      }
    }
  })  
  
  
  
  ;
  
  
  
  

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/deals');
});
